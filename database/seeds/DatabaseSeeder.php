<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        DB::table('categories')->delete();
        $this->call('CategoriesTableSeeder');
        
        DB::table('products')->delete();
        $this->call('ProductsTableSeeder');
        
        DB::table('product_category')->delete();
        $this->call('ProductCatogoryTableSeeder');
    }
}
