<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
                    [
                        'category_code'=> 'CAT-0001',
                        'name'=>'Electronic',
                        'parent_category_id'=>null,
                    ],
                    [
                        'category_code'=> 'CAT-0002',
                        'name'=>'Dairy',
                        'parent_category_id'=>null,
                    ]
                ];
        Category::insert($data);
    }
}
