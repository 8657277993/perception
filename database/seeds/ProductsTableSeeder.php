<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
                    [
                        'product_code'=> 'PROD-0001',
                        'name'=>'Samsung',
                    ],
                    [
                        'product_code'=> 'PROD-0002',
                        'name'=>'Milk',
                    ]
            ];
        Product::insert($data);
    }
}
