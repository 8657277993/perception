<?php

use Illuminate\Database\Seeder;
use App\ProductCategory;

class ProductCatogoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $data = [
                    [
                        'category_id'=> 1,
                        'product_id'=>1,
                    ],
                    [
                        'product_code'=> 2,
                        'name'=>2,
                    ]
            ];
        ProductCategory::insert($data);
    }
}
