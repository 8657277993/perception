@extends('layouts.admin')
@section('page-content')

<div class="content-wrapper">
    <!--<section class="content-header">
        <h1>Data Tables<small>advanced tables</small></h1>
    </section>-->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                 <div class="hide" id="success_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="fa fa-times"></i>
                    </button>
                    <p id="success_response_msg"><i class="icon fa fa-check"></i></p>
                </div>
                
            @if(Session::has('message'))
               <div class="alert {{ Session::get('alert-class') }} alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                       <i class="fa fa-times"></i>
                   </button>
                   <p><i class="{{ Session::get('icon-class') }}"></i> {{ Session::get('message') }}</p>
               </div>
            @endif
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box  border-top-none">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category List</h3>
                        <div class="box-tools pull-right">
                            <!--<a href="#" class="btn btn-sm btn-info btn-flat pull-right">&nbsp;</a>-->
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body table-responsive no-padding">
                            <div class="box-header">
                                <div class="card-body">
                                    <table id="example" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Category Code</th>
                                                <th>Name</th>
                                                <th>Parent Category</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($data) > 0)
                                            @foreach($data as $row)
                                                <tr>
                                                    <td>{{$row->category_code}}</td>
                                                    <td>{{$row->name}}</td>
                                                    <td>{{$row->getCategoryName($row->parent_category_id)}}</td>
                                                    
                                                    <td>
                                                        
                                                        <a href="{!! url('get-all-category', ['recordId' => $row->id,'process' => 'view']) !!}"  title="View" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                                                        <a href="{!! url('get-all-category', ['recordId' => $row->id,'process' => 'edit']) !!}"  title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
                                                        @if($row->deleted_at == null)
                                                        <a href="{!! url('delete-category', ['recordId' => $row->id,'process' => 'delete']) !!}" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>
                                                        @else
                                                        <a href="{!! url('delete-category', ['recordId' => $row->id,'process' => 'restore']) !!}" title="Restore" data-toggle="tooltip"><i class="fa fa-undo"></i></a></td>
                                                        @endif
                                                    
                                                </tr>
                                            @endforeach;
                                            @endif;
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 col-xs-12">
                <div class="box  border-top-none">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>
                    <div class="box-body">
                          <div class="box-body">
                        {!! Form::open(['url' => 'store-category', 'id' => 'addcategory', 'method' => 'post', 'class' => '']) !!}
                        {!! Form::hidden("id",@$editRecord->id,['class' => 'form-control','id' =>  "id"]) !!}
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('category_id','Category') !!}
                                        {!! Form::select('parent_category_id', $arrCategories, @$editRecord->parent_category_id, ['class' => 'form-control', 'id' => 'parent_category_id']) !!}
                                    </div>
                                </div>
                                
                                 <div class="col-md-12 col-xs-12">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('name','Name') !!}
                                        {!! Form::text('name',@$editRecord->name,['class' => 'form-control']) !!}
                                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('Category_Code',$category_code_lable) !!}
                                        {!! Form::text('category_code',@$editRecord?$editRecord->category_code:$category_code,['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>
                                </div>
                                
                            </div>
                           
                           
                            <div class="box-footer">
                            @if($submitbtn == '' || $submitbtn == 'edit')
                                {!! Form::submit('Submit',['class' => 'btn btn-info pull-right']) !!}&nbsp;&nbsp;
                                <a href="{{url('/get-all-category')}}" class="btn btn-default">Back</a>
                            @else
                                <a href="{{url('/get-all-category')}}" class="btn btn-default">Back</a>
                            @endif
                         </div>  
                             
                        
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 1, "desc" ]]
    } );
} );
</script>
@endsection 