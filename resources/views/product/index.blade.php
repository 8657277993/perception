@extends('layouts.admin')
@section('page-content')
<div class="content-wrapper">
    <!--<section class="content-header">
        <h1>Data Tables<small>advanced tables</small></h1>
    </section>-->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                 <div class="hide" id="success_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="fa fa-times"></i>
                    </button>
                    <p id="success_response_msg"><i class="icon fa fa-check"></i></p>
                </div>
                
            @if(Session::has('message'))
               <div class="alert {{ Session::get('alert-class') }} alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                       <i class="fa fa-times"></i>
                   </button>
                   <p><i class="{{ Session::get('icon-class') }}"></i> {{ Session::get('message') }}</p>
               </div>
            @endif
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box  border-top-none">
                    <div class="box-header with-border">
                        <h3 class="box-title">Product List</h3>
                        <div class="box-tools pull-right">
                            <!--<a href="#" class="btn btn-sm btn-info btn-flat pull-right">&nbsp;</a>-->
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body table-responsive no-padding">
                            <div class="box-header">
                                <div class="card-body">
                                    <table id="example" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Product Code</th>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            @if(count($data) > 0)
                                            @foreach($data as $row)
                                                @php
                                                    $imgeurl = asset('/public/images/original/'.$row->image);
                                                @endphp
                                                <tr>
                                                 
                                                    <td><img src="{{$imgeurl}}" alt="project_gallery_img" style="width:50px;"/></td>
                                                    <td>{{$row->product_code}}</td>
                                                    <td>{{$row->name}}</td>
                                                    <td>
                                                        <a href="{!! url('get-all-product', ['recordId' => $row->id,'process' => 'view']) !!}"  title="View" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                                                        <a href="{!! url('get-all-product', ['recordId' => $row->id,'process' => 'edit']) !!}"  title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
                                                        @if($row->deleted_at == null)
                                                        <a href="{!! url('delete-product', ['recordId' => $row->id,'process' => 'delete']) !!}" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>
                                                        @else
                                                        <a href="{!! url('delete-product', ['recordId' => $row->id,'process' => 'restore']) !!}" title="Restore" data-toggle="tooltip"><i class="fa fa-undo"></i></a></td>
                                                        @endif
                                                    
                                                </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
              <div class="col-md-4 col-xs-12">
                <div class="box  border-top-none">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Product</h3>
                    </div>
                    <div class="box-body">
                          <div class="box-body">
                        {!! Form::open(['url' => 'store-product', 'id' => 'addproduct', 'method' => 'post', 'class' => '','enctype'=>'multipart/form-data']) !!}
                        {!! Form::hidden("id",@$editRecord->id,['class' => 'form-control','id' =>  "id"]) !!}
                            <div class="row">
                               
                                <?php //echo '<pre>'; print_r($editRecord->getCategoryId);?>
                                
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group has-feedback">
                                        @php
                                            $categoryArray = [];
                                        @endphp
                                        @if(!empty($editRecord))
                                            @if(count($editRecord['product_category']) > 0)
                                                @foreach($editRecord['product_category'] as $key => $value)
                                                    @php
                                                        $categoryArray[] = $value->category_id;
                                                    @endphp
                                                @endforeach
                                            @endif
                                        @endif
                                        
                                        {!! Form::label('category_id','Category') !!}
                                        {!! Form::select('category_id[]', $arrCategories, $categoryArray, ['class' => 'form-control select2','multiple'=>'multiple', 'id' => '']) !!}
                                        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                
                                 <div class="col-md-12 col-xs-12">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('name','Name') !!}
                                        {!! Form::text('name',@$editRecord->name,['class' => 'form-control']) !!}
                                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('Category_Code',$code_lable) !!}
                                        {!! Form::text('product_code',@$editRecord?$editRecord->product_code:$product_code,['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>
                                </div>
                                
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('file','File') !!}
                                        {!! Form::hidden("exiting_image",@$editRecord->image,['class' => 'form-control','id' =>  ""]) !!}
                                        <input type="file" class="show form-control" name="file_name" id="file_name" onchange="readURL(this);">
                                        <img id="blah" src="#" alt="your image" width='100px' />
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="box-footer">
                            @if($submitbtn == '' || $submitbtn == 'edit')
                                {!! Form::submit('Submit',['class' => 'btn btn-info pull-right']) !!}&nbsp;&nbsp;
                                <a href="{{url('/get-all-product')}}" class="btn btn-default">Back</a>
                                @else
                                <a href="{{url('/get-all-product')}}" class="btn btn-default">Back</a>
                            @endif
                         </div>  
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>
            </div>
            
          
        </div>
    </section>
</div>
<script type="text/javascript">
$(document).ready(function() {
     $('.select2').select2()
    $('#example').DataTable( {
        "order": [[ 1, "desc" ]]
    } );
} );

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#blah').attr('src', e.target.result).width(150).height(200);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
</script>
@endsection 