<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', function () {
    return view('dashboard');
});


Route::get('/get-all-category/{id?}/{process?}', 'CategoryController@getAllCategory');
Route::post('/store-category', 'CategoryController@storeCategory');
Route::get('/delete-category/{id}/{process}', 'CategoryController@deleteCategory');

Route::get('/get-all-product/{id?}/{process?}', 'ProductController@getAllProduct');
Route::post('/store-product', 'ProductController@storeProduct');
Route::get('/delete-product/{id}/{process}', 'ProductController@deleteProduct');

