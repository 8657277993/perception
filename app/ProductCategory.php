<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //
    protected $table = 'product_category';
    public $field;

    public function addProductCategory() {
        $arrResp = [];
        $status = false;
        $message = '';
        $addData = array();
        $lastInsertId = 0;
        try {
            $inputData = $this->field['data'];
            $commanObj = new ProductCategory();
            $addData['product_id'] = $inputData['product_id'];
            $addData['category_id'] = $inputData['category_id'];
            if (!empty($inputData['id'])) {
                $query = self::query();
                $query->where('id', $inputData['id']);
                $lastInsertId = $query->update($addData);
                $lastInsertId = $inputData['id'];
                $message = 'Product has been update sucessfully.';
            } else {
                $lastInsertId = $commanObj->insertGetId($addData);
                $message = 'Product has been added sucessfully.';;
            }
            if ($lastInsertId) {
                $status = true;
            } else {
                $status = false;
                if (!empty($inputData['id'])) {
                    $message = 'Unable to update product.';
                } else {
                    $message = 'Unable to add product.';;
                }
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }
}
