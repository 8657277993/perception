<?php

namespace App\Providers;
use App\Repositories\Todo\EloquentCategory;
use App\Repositories\Todo\CategoryRepositories;

use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;
use App\Product;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
//        $this->app->singleton(CategoryRepositories::class, EloquentCategory::class); 
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Str::macro('getNextAutoNumber', function ($model,$columname, $prefix) {
            $data = $model::orderBy('id','desc')->first();
            if(!$data){
                $og_length = $length;
                $nextAutoNumber = $prefix."1";
            }else{
                $autoNumber = $data->$columname+1;
                $nextAutoNumber = $prefix.$autoNumber;
            }
            return $nextAutoNumber;
        });
        
    }
}
