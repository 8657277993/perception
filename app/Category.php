<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model {

    //
    protected $table = 'categories';
    public $field;

    public static function getCategories($select = true) {
        $arrResp = [];
        $arrData = [];
        $status = 0;
        $message = '';
        $arrParent = [];
        if ($select) {
            $arrParent[''] = 'Select Category';
        }

        try {

            $arrData = self::where('parent_category_id', null)
                    ->where('deleted_at', null)
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');
            if (count($arrData) > 0) {
                foreach ($arrData as $key => $value) {
                    $arrParent[$key] = $value;
                }
                $message = 'Categories list';
            } else {
                $message = 'Categories not available!';
            }
            $status = 1;
        } catch (Exception $ex) {
            $status = 0;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['data'] = $arrParent;

        return $arrResp;
    }

    public function getAllCategory() {
        $arrResp = [];
        $arrData = [];
        $status = 0;
        $message = '';
        try {
            $arrData = Category::get();
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['data'] = $arrData;

        return $arrResp;
    }

    Public function getCategoryName($parentid = 0) {
        $name = '';
        $response = Category::where('id', $parentid)->first();
        if (empty($response)) {
            $name = "-";
        } else {
            $name = $response['name'];
        }
        return $name;
    }

    public function addUpdateCategory() {
        $arrResp = [];
        $status = false;
        $message = '';
        $addData = array();
        $lastInsertId = 0;
        try {
            $inputData = $this->field['data'];
            $commanObj = new Category();
            $addData['id'] = $inputData['id'];
            $addData['name'] = $inputData['name'];
            if (isset($inputData['category_code']) && !empty($inputData['category_code'])) {
                $addData['category_code'] = $inputData['category_code'];
            }
//            if (isset($inputData['parent_category_id']) && !empty($inputData['parent_category_id'])) {
            $addData['parent_category_id'] = $inputData['parent_category_id'];
//            }

            if (!empty($inputData['id'])) {
                $query = self::query();
                $query->where('id', $inputData['id']);
                $lastInsertId = $query->update($addData);
                $lastInsertId = $inputData['id'];
                $message = 'Category has been update sucessfully.';
            } else {
                $lastInsertId = $commanObj->insertGetId($addData);
                $message = 'Category has been added sucessfully.';
                ;
            }
            if ($lastInsertId) {
                $status = true;
            } else {
                $status = false;
                if (!empty($inputData['id'])) {
                    $message = 'Unable to update category.';
                } else {
                    $message = 'Unable to add category.';
                    ;
                }
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }

    public function deleteResotreCategory() {
        $arrResp = [];
        $status = false;
        $addData = array();
        $lastInsertId = 0;
        try {
            $id = $this->field['id'];
            $process = $this->field['process'];
            $commanObj = new Category();
            $addData['deleted_at'] = date("Y-m-d h:i:s");
            if ($process == 'restore') {
                $addData['deleted_at'] = null;
            }
            if (!empty($id)) {
                $query = self::query();
                $query->where('id', $id);
                $lastInsertId = $query->update($addData);
                
                if ($process == 'restore') {
                    $query = self::query();
                    $query->where('parent_category_id', $id);
                    $lastInsertId = $query->update($addData);
                }
                
                $lastInsertId = $id;
                if ($process == 'restore') {
                    $message = 'Category has been restore sucessfully.';
                } else {
                    $message = 'Category has been deleted sucessfully.';
                }
            }
            if ($lastInsertId) {
                $status = true;
            } else {
                if ($process == 'restore') {
                    $message = 'Unable to restore category.';
                } else {
                    $message = 'Unable to deleted category.';
                }
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }

}
