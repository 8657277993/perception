<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Product;
use App\Category;
use App\ProductCategory;
use DB;
use File;
use Illuminate\Support\Str;
use Validator;
class ProductController extends Controller {

    public function getAllProduct($id = 0, $process = '') {
        $submitbtn = $process;
        $this->viewData['pageTitle'] = '';
        
        $codeLable = 'Next Product Code';
        $product_code = Str::getNextAutoNumber('\App\Product','id','PROD-000');

        $category = new Category();
        $arrCategoriesResp = $category->getCategories(false);
        $this->viewData['arrCategories'] = $arrCategoriesResp['data'];

        $product = new Product();
        $productData = $product->getAllProduct();
        $this->viewData['data'] = $productData['data'];

        // view or edit
        $editdata = '';
        $submitbtn = $process;
        if(!empty($id)){
            $codeLable = 'Product Code';
            $product = new Product();
            $responsedata =  $product->getProductCategory($id);
            $editdata = $responsedata['data'][0];
        }
        
        $this->viewData['editRecord'] = $editdata;
        $this->viewData['submitbtn'] = $submitbtn;
        $this->viewData['code_lable'] = $codeLable;
        $this->viewData['product_code'] = $product_code; 
        return view('product.index', $this->viewData);
    }

    public function storeProduct(Request $request) {
             // Validation => START
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'category_id' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            // Validation => END
        DB::beginTransaction();
        try {

            $input = $request->all();
            $arrResp = [];
            // If user chage by using inspect element that purpose we are implement below code
            if(isset($input['id']) && empty($input['id'])){
                $input['product_code'] = Str::getNextAutoNumber('\App\Product','id','PROD-000');
            }
           
            $fileName = $input['exiting_image'];
            if (!empty($request->file('file_name'))) {
                $image = $request->file('file_name');
                $fileName = $image->getClientOriginalName();
            }
            $input['image'] = $fileName;
            
            // Create new object.
            $product = new Product();
           
            // Pass Data To Model.
            $product->field['data'] = $input;
             
            $arrResp = $product->addUpdateProduct();
            if($arrResp['status']){
                $productCategoryObj = new ProductCategory();
                foreach($input['category_id'] as $key => $value){
                    $pivotData['product_id'] = $arrResp['lastInsertId'];
                    $pivotData['category_id'] = $value;
                    $productCategoryObj->field['data'] = $pivotData;
                    $productCategoryrRes = $productCategoryObj->addProductCategory();
                }
                if($productCategoryrRes['status']){
                    DB::commit();
                    if (!empty($request->file('file_name'))) {
                        $original = public_path() . '/images/' . 'original';
                        $image->move($original, $fileName);
                    }
                    $status = true;
                }
            }
            
            Session::flash('message', $arrResp['message']);
            Session::flash('alert-class', 'alert-success');
            Session::flash('icon-class', 'icon fa fa-check');
        } catch (\Exception $ex) {
            DB::rollback();
            Session::flash('message', $ex->getMessage());
            Session::flash('alert-class', 'alert-danger');
            Session::flash('icon-class', 'icon fa fa-ban');
        }
        return redirect('/get-all-product');
    }

    public function deleteProduct($id, $process) {
        try {
            $arrResp = [];
            // Create new object
            $commonObj = new Product();
            // Pass Data To Model
            $commonObj->field['id'] = $id;
            $commonObj->field['process'] = $process;
            $arrResp = $commonObj->deleteResotreProduct();
            Session::flash('message', $arrResp['message']);
            Session::flash('alert-class', 'alert-success');
            Session::flash('icon-class', 'icon fa fa-check');
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            Session::flash('alert-class', 'alert-danger');
            Session::flash('icon-class', 'icon fa fa-ban');
        }
        return redirect('/get-all-product');
    }

}
