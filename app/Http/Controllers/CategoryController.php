<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Category;
use Illuminate\Support\Str;
use Validator;

class CategoryController extends Controller
{
//     private $category; 
    
    public function getAllCategory($id = 0, $process = '') {
        $this->viewData['pageTitle'] = '';
         
        $category = new Category();
        $arrCategoriesResp = $category->getCategories();
        $this->viewData['arrCategories'] = $arrCategoriesResp['data'];
        
       // view or edit
        $parent_category_id = 0;
        $editdata = '';
        $submitbtn = $process;
        $categoryCodeLable = 'Next Category Code';
        $category_code = Str::getNextAutoNumber('\App\Category','id','CAT-000');
        if(!empty($id)){
            $categoryCodeLable = 'Category Code';
            $editdata = Category::find($id);
//            $parent_category_id = $editdata->parent_category_id;
        }
        $cateoryData = $category->getAllCategory();
        $this->viewData['data'] = $cateoryData['data'];
//        $this->viewData['parent_category_id'] = $parent_category_id;
        $this->viewData['editRecord'] = $editdata;
        $this->viewData['submitbtn'] = $submitbtn;
        $this->viewData['category_code_lable'] = $categoryCodeLable;
        $this->viewData['category_code'] = $category_code; 
        
        return view('category.index', $this->viewData);
    }
    
    
    public function storeCategory(Request $request) {
        try {
            
            
            $input = $request->all();
            // Validation => START
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            // Validation => END
            
            $arrResp = [];
            // Create new object.
            $category = new Category();  
            // Pass Data To Model.
            // If user chage by using inspect element that purpose we are implement below code
            if(isset($input['id']) && empty($input['id'])){
                $input['category_code'] = Str::getNextAutoNumber('\App\Category','id','CAT-000');
            }
            $category->field['data'] = $input;
            $arrResp = $category->addUpdateCategory();
            
            Session::flash('message', $arrResp['message']); 
            Session::flash('alert-class', 'alert-success'); 
            Session::flash('icon-class', 'icon fa fa-check');
            
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage()); 
            Session::flash('alert-class', 'alert-danger'); 
            Session::flash('icon-class', 'icon fa fa-ban');
        }
        return redirect('/get-all-category');
    }
    
    
    public function deleteCategory($id, $process) {
        try {

            $arrResp = [];
            // Create new object
            $commonObj = new Category();
            // Pass Data To Model
            $commonObj->field['id'] = $id;
            $commonObj->field['process'] = $process;
            $arrResp = $commonObj->deleteResotreCategory();
            Session::flash('message', $arrResp['message']); 
            Session::flash('alert-class', 'alert-success'); 
            Session::flash('icon-class', 'icon fa fa-check');
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage()); 
            Session::flash('alert-class', 'alert-danger'); 
            Session::flash('icon-class', 'icon fa fa-ban');
        }
        return redirect('/get-all-category');
        
    }
    
   
}
