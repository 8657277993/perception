<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    //
    protected $table = 'products';
    public $field;

    public function getAllProduct() {
        $arrResp = [];
        $arrData = [];
        $status = 0;
        $message = '';
        try {
            $arrData = Product::get();
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['data'] = $arrData;

        return $arrResp;
    }

    public function addUpdateProduct() {
        $arrResp = [];
        $status = false;
        $message = '';
        $addData = array();
        $lastInsertId = 0;
        try {
            $inputData = $this->field['data'];
            $commanObj = new Product();
            $addData['id'] = $inputData['id'];
            $addData['image'] = $inputData['image'];
            $addData['name'] = $inputData['name'];
            if (isset($inputData['product_code']) && !empty($inputData['product_code'])) {
                $addData['product_code'] = $inputData['product_code'];
            }
            if (!empty($inputData['id'])) {
                $query = self::query();
                $query->where('id', $inputData['id']);
                $lastInsertId = $query->update($addData);
                $lastInsertId = $inputData['id'];
                $message = 'Product has been update sucessfully.';
            } else {
                $lastInsertId = $commanObj->insertGetId($addData);
                $message = 'Product has been added sucessfully.';
                ;
            }
            if ($lastInsertId) {
                $status = true;
            } else {
                $status = false;
                if (!empty($inputData['id'])) {
                    $message = 'Unable to update product.';
                } else {
                    $message = 'Unable to add product.';
                    ;
                }
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }

    public function deleteResotreProduct() {
        $arrResp = [];
        $status = false;
        $addData = array();
        $lastInsertId = 0;
        try {
            $id = $this->field['id'];
            $process = $this->field['process'];
            $commanObj = new Product();
            $addData['deleted_at'] = date("Y-m-d h:i:s");
            if ($process == 'restore') {
                $addData['deleted_at'] = null;
            }
            if (!empty($id)) {
                $query = self::query();
                $query->where('id', $id);
                $lastInsertId = $query->update($addData);
                $lastInsertId = $id;
                if ($process == 'restore') {
                    $message = 'Product has been restore sucessfully.';
                } else {
                    $message = 'Product has been deleted sucessfully.';
                }
            }
            if ($lastInsertId) {
                $status = true;
            } else {
                if ($process == 'restore') {
                    $message = 'Unable to restore product.';
                } else {
                    $message = 'Unable to deleted product.';
                }
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }

    public function getCategory() {
//        exit('in model');
//        return $this->hasMany('App\ProductCategory', 'product_id','id');
//        return $this->belongsToMany('App\ProductCategory', 'product_id','id');
        return $this->belongsToMany('Category::class', 'product_category','id','category_id');
//        return DB::table('product_category')->where('product_id',$id)->get();
    }
    
     public function product_category() {
        return $this->hasMany('App\ProductCategory')
                                ->select('id','category_id','product_id');
    }
    
    public static function getProductCategory($productId=0) {
        $name       = '';
        $arrData    = [];
        $arrResp    = [];
        $status     = 0;
        $message    = '';
        try {
            
            $query = self::query();
            $query->select('products.*');
            $query->with('product_category');
            $query->where('id','=',$productId);
            $query->orderBy('id', 'asc');
            $arrData = $query->get();
            $status = true;
            $message = 'success';
        } catch (Exception $ex) {
            $status = 0;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['data'] = $arrData;
        
        return $arrResp;
    }

}
